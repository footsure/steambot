<?php

use App\Steam\SteamAPIService;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {

            $table->increments('id');

            $table->enum('type', ['outcoming', 'incoming']) -> default(null);

            $offerStates = array_unique(array_merge(['queued','created','failed'], array_values(\App\Steam\SteamAPIService::$offerStates)));

            $table->enum('status', $offerStates) -> default(null);

            $table->string('stateCode') -> nullable();

            $table->integer('own_id') -> nullable() -> unsigned()  -> default(null);
            $table->integer('mediator_id') -> nullable() -> unsigned()  -> default(null);

            $table->string('partnerAccountId') -> nullable();
            $table->string('accessToken') -> nullable();

            $table->string('item_market_hash_name') -> nullable();
            $table->string('item_market_instance_id') -> nullable();

            $table->integer('item_id') -> nullable() -> unsigned()  -> default(null);

            $table->timestamps();
            
            $table->foreign('own_id')      ->references('id') ->on('bots')  ->onDelete('SET NULL') ->onUpdate('SET NULL');
            $table->foreign('mediator_id') ->references('id') ->on('bots')  ->onDelete('SET NULL') ->onUpdate('SET NULL');
            $table->foreign('item_id')     ->references('id') ->on('items') ->onDelete('SET NULL') ->onUpdate('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('offers');
    }
}
