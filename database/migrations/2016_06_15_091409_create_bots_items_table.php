<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBotsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bots_items', function (Blueprint $table) {
            $table->integer('bots_id')->unsigned()->nullable();
            $table->foreign('bots_id')->references('id')
                ->on('bots')->onDelete('cascade');

            $table->integer('items_id')->unsigned()->nullable();
            $table->foreign('items_id')->references('id')
                ->on('items')->onDelete('cascade');

            $table->boolean('available')->default(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bots_items');
    }
}
