<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table -> increments('id');
            $table -> string('type') -> nullable()  -> default(null);
            $table -> integer('defindex') -> nullable()  -> default(null);
            $table -> integer('parent_defindex') -> nullable()  -> default(null);
            $table -> string('name');
            $table -> string('tech_name') -> nullable();
            $table -> string('parent_name') -> nullable();
            $table -> string('hash_name');
            $table -> index('hash_name');
            $table -> enum('exterior', ['factory_new','minimal_wear','field_tested','well_worn','battle_scarred']) ->nullable() ->default(null);
            $table -> enum('rarity', ['default','common','uncommon','rare','mythical','legendary','ancient','immortal','unusual']) ->nullable() ->default(null);
            $table -> integer('stattrak') -> default(0);
            $table -> text('images') -> nullable();
            $table -> text('lang')   -> nullable();
            $table -> integer('check') -> default(0);
            $table -> softDeletes();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
