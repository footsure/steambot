<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemInstanceIdFieldIntoBotsItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bots_items', function (Blueprint $table) {
            $table->string('item_instance_id')->after('items_id');
            $table->index('item_instance_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bots_items', function (Blueprint $table) {
            //
        });
    }
}
