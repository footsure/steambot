<?php

namespace App;

use App\Steam\SteamAPIService;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model {

    use SoftDeletes;
    
    protected $table = 'items';

    /*The attributes that should be mutated to dates.*/
    protected $dates = ['deleted_at'];
    
    protected $touches = ['bots'];

    public function bots() {
        return $this->belongsToMany('App\Bot', 'bots_items', 'items_id', 'bots_id')
                    ->withPivot(['available', 'item_instance_id'])
                    ->withTimestamps();
    }

    public function offers() {
        return $this->hasMany('App\Offer');
    }

    /**
     * Mapping newfound items to similar internal ones.
     *
     * expression $items->keyBy('column')->only(['key1', 'key2', 'key3']) where $items is instance of \Illuminate\Database\Eloquent\Collection
     * is not working as expected, because ->getKey() method called on \Illuminate\Database\Eloquent\Model instance inside ->getDictionary()
     * return primary model key instead of its index in an associative array (returned from ->keyBy('column'))
     *
     * the difference between keyBy & groupBy: in case of keyBy if multiple items have the same key, only the last one will appear in the new collection
     * while groupBy returns associative array of \Illuminate\Database\Eloquent\Collection instances, each contains all items from original array having the same key
     *
     * besides expression $items->groupBy('column')->only(['key1', 'key2', 'key3']) where $items is instance of \Illuminate\Database\Eloquent\Collection
     * is also not working, because throws error "Method getKey does not exist"
     *
     * @param  \Illuminate\Database\Eloquent\Collection  &$added [item_instance_id, market_hash_name, name]
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getMatchedItemsIds($added) {

        // dump_r($added);

        $added = collect($added);

        $hash_names = $added->pluck(2)->unique();
        // dump_r($hash_names);

        $items = self::whereIn('hash_name', $hash_names)->get();

        $items = $items->keyBy(function ($item, $key) {
            return $item['hash_name'] . SteamAPIService::$exteriors[$item['exterior']];
        });

        // dump_r($items);

        $items = array_intersect_key( $items->all(), $added->pluck(1)->flip()->all() );

        $results = [];

        $added = $added -> groupBy(1);

        // dump_r($added);

        foreach ($items as $hash_name => $item) {
            foreach ($added[$hash_name] as $level => $steamItem) {
                $results[$level][$item->id] = ['item_instance_id' => $steamItem[0] ];
            }
        }
        return $results;

    }

}
