<?php

namespace App\Steam;

use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Cookie\CookieJar;

use App\Exceptions\Exception;

trait SteamTradingAPI {

	public function makeOffer($options) {

		$tradeOffer = array(
		    'newversion' => TRUE,
		    'version' => 2,
		    'me' => array('assets' => $options['itemsFromMe'], 'currency' => array(), 'ready' => FALSE ),
		    'them' => array('assets' => $options['itemsFromThem'], 'currency' => array(), 'ready' => FALSE )
	  	);

	  	$formFields = array(
		    'serverid' => 1,
		    'sessionid' => $this->sessionId,
		    'partner' => isset($options['partnerSteamId']) ? $options['partnerSteamId'] : $this->toSteamID($options['partnerAccountId']),
		    'tradeoffermessage' => isset($options['message']) ? $options['message'] : '',
		    'json_tradeoffer' => json_encode($tradeOffer)
	  	);	

	  	$query = array(
		    'partner' => isset($options['partnerAccountId']) ? $options['partnerAccountId'] : $this->toAccountID($options['partnerSteamId'])
		);

	  	if(isset($options['accessToken'])) {
	  		$formFields['trade_offer_create_params'] = json_encode(array('trade_offer_access_token'=>$options['accessToken']));
	  		$query['token'] = $options['accessToken'];
	  	}

	  	$referer = '';
	  	if(isset($options['counteredTradeOffer'])) {
	  		$formFields['tradeofferid_countered'] = $options['counteredTradeOffer'];
	  		$referer = 'https://steamcommunity.com/tradeoffer/'.$options['counteredTradeOffer'].'/';
	  	} else {
	  		$referer = 'https://steamcommunity.com/tradeoffer/new/?'.http_build_query($query);
	  	}

	  	$headers['referer'] = $referer;

		$reqOptions['form_params'] = $formFields;

	  	// $reqOptions['headers'] = $this->mobileHeaders;
	  	$reqOptions['headers'] = $headers;
		$reqOptions['cookies'] = $this->jar;
		$reqOptions['timeout'] = 5;
		
		// dump_r($reqOptions);

	  	$response = $this->client->request('POST', 'https://steamcommunity.com/tradeoffer/new/send', $reqOptions);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error making offer. Server response code: ".$response->getStatusCode());

		$response = json_decode($response->getBody());

		if($response && isset($response->strError))
			throw new Exception("Error making offer: ".$response->strError);

		// r($response);

	  	return $response;
	}

	public function acceptOffer($tradeOfferId, $partnerId) {

		$params['tradeofferid'] = $tradeOfferId;
		$params['sessionid']    = $this->sessionId;
		$params['serverid']     = 1;
		$params['partner']      = $partnerId;

		$options['form_params'] = $params;
		$options['cookies']     = $this->jar;

		$url     = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/accept';
        $referer = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/';

	  	$headers['referer']     = $referer;
	  	$options['headers']     = $headers;

	  	// dump_r($options);

	  	$response = $this->client->request('POST', $url, $options);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error accepting offer. Server response code: ".$response->getStatusCode());

		$response = json_decode($response->getBody());

		if($response && isset($response->strError))
			throw new Exception("Error accepting offer: ".$response->strError);

		// r($response);

	  	return $response;
	}

	public function declineOffer($tradeOfferId) {

		$params['tradeofferid'] = $tradeOfferId;
		$params['sessionid']    = $this->sessionId;
		$params['serverid']     = 1;
		$params['partner']      = '326971648';

		$options['form_params'] = $params;
		$options['cookies']     = $this->jar;

		$url     = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/decline';
        $referer = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/';

	  	$headers['referer']     = $referer;
	  	$options['headers']     = $headers;

	  	$response = $this->client->request('POST', $url, $options);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error declining offer. Server response code: ".$response->getStatusCode());

		$response = json_decode($response->getBody());

		if($response && isset($response->strError))
			throw new Exception("Error declining offer: ".$response->strError);

		// r($response);

	  	return $response;
	}

	public function cancelOffer($tradeOfferId) {

		$params['tradeofferid'] = $tradeOfferId;
		$params['sessionid']    = $this->sessionId;
		$params['serverid']     = 1;
		$params['partner']      = '326971648';

		$options['form_params'] = $params;
		$options['cookies']     = $this->jar;

		$url     = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/cancel';
        $referer = 'https://steamcommunity.com/tradeoffer/' . $tradeOfferId . '/';

	  	$headers['referer']     = $referer;
	  	$options['headers']     = $headers;

		$response = $this->client->request('POST', $url, $options);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error canceling offer. Server response code: ".$response->getStatusCode());

		$response = json_decode($response->getBody());

		if($response && isset($response->strError))
			throw new Exception("Error canceling offer: ".$response->strError);

		// r($response);

	  	return $response;
	}

	public function getIncomingOffers() {
	    return $this->_parseTradeOffers($this->offersRequest(), false);
	}

	public function getIncomingOfferHistory() {
	    return $this->_parseTradeOffers($this->offersRequest('', 'history'), false);
	}

	public function getSentOffers() {
	    return $this->_parseTradeOffers($this->offersRequest('sent'), true);
	}

	public function getSentOfferHistory() {
	    return $this->_parseTradeOffers($this->offersRequest('sent', 'history'), true);
	}

	private function offersRequest ($s = '', $h = '') {
		$url = 'http://steamcommunity.com/id/'.$this->model->accountName.'/tradeoffers/'.$s;
		if (!empty($h)) 
			$options['query'] = ['history'=>1];
			$cookies = array_column($this->jar->toArray(), 'Value', 'Name');	
			// dump_r($this->jar->toArray());
			$options['cookies'] = CookieJar::fromArray($cookies, 'steamcommunity.com');

		$response = $this->client->request('GET', $url, $options);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error getting offers list. Server response code: ".$response->getStatusCode());

		$response = (string)$response->getBody();
		if (empty($response)) throw new Exception("Error requesting confirmations list: Malformed response");
		return $response;
	}

	private function _getOfferStateCode ($message) {
		
		$stateslist = [
			'Awaiting Mobile Confirmation'    => 'k_ETradeOfferStateCreatedNeedsConfirmation',
			'Awaiting Email Confirmation'     => 'k_ETradeOfferStateCreatedNeedsConfirmation',
			'Trade Offer Canceled'            => 'k_ETradeOfferStateCanceled',
			'Trade Declined'                  => 'k_ETradeOfferStateDeclined',
			'On hold'                         => 'k_ETradeOfferStateInEscrow',
			'Trade Accepted'                  => 'k_ETradeOfferStateAccepted',
			'Items Now Unavailable For Trade' => 'k_ETradeOfferStateInvalidItems',
			'Counter Offer Made'              => 'k_ETradeOfferStateCountered',
			'Trade Offer Expired'             => 'k_ETradeOfferStateExpired',
		];
		foreach ($stateslist as $key => $state)
			if (strpos($message, $key) !== false)
				return $state;
		        return 'k_ETradeOfferStateInvalid';
	}

	private function _parseTradeOffers($html, $isOurOffer) {

		$crawler = new Crawler($html);

		$tradeOffers = $crawler -> filter('div[id^=tradeofferid_]') -> each( function ($node, $i) use ($isOurOffer) {

			$offer[ 'id' ] = str_replace('tradeofferid_', '', $node->attr('id'));
			$offer['type'] = $isOurOffer ? 'outcoming' : 'incoming';

			$class = $isOurOffer ? 'secondary' : 'primary';
			
			$offer['otherAccountId'] = $node->filter(".tradeoffer_items.$class a[data-miniprofile]")->first()->attr('data-miniprofile');
			
			$arr = $node->filter('.tradeoffer_message .quote')->each(function($node){return trim($node->text());});
			if (!empty($arr)) $offer['msg'] = $arr[0];

			$arr = $node->filter('.tradeoffer_footer')->each(function($node){if(!empty($node->text())) return trim($node->text());});
			if (!empty($arr)) {
				$offer['exp'] = $arr[0];
				$pos = strpos($offer['exp'], 'Offer expires on ');
				$offer['exp'] = substr($offer['exp'], $pos+17);
				$offer['exp'] = strtotime($offer['exp']);
			}

			$node -> filter('.tradeoffer_items_banner ') -> first() -> each( function ($node) use (&$offer) {

				$message = trim($node->text());

				$offer['state'] = $this->_getOfferStateCode($message);
				$offer['status'] = self::$offerStates[$offer['state']];

				if ( $offer['status'] == 'hold' ) {

					$parts = explode('.', $message);
					$acceptedString = trim($parts[0]);
					$acceptedDate = \DateTime::createFromFormat('M j, Y @ g:ia', str_replace('Trade Accepted ', '', $acceptedString));
					if ($acceptedDate !== false)
					    $offer['acceptedDate'] = $acceptedDate->getTimestamp();
					$escrowString = trim($parts[1]);
					$escrowDate = \DateTime::createFromFormat('M j, Y @ g:ia', str_replace('On hold until ', '', $escrowString));
					if ($escrowDate !== false)
					    $offer['escrowDate'] = $escrowDate->getTimestamp();
				}
			});
			if (!isset($offer['state'])) {

				$offer['state'] = 'k_ETradeOfferStateActive';
				$offer['status'] = self::$offerStates[$offer['state']];
			}
			return $offer;
		});

		// \Illuminate\Support\Facades\Storage::put(date('h-i-s').'.txt', json_encode($tradeOffers));

		return array_column($tradeOffers, null, 'id');
	}
}