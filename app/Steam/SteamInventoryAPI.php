<?php

namespace App\Steam;

use App\Exceptions\Exception;
use App\Exceptions\CustomException;

trait SteamInventoryAPI {

	/**
	 * @param array $options {
 	 *     Array of query parameters.
 	 *     @type string       $appId          is the Steam AppID.
 	 *     @type string       $contextId      is the inventory context Id.
 	 *     @type string       $language       (optional) is the language for item descriptions.
 	 *     @type string       $tradableOnly   (optional) is a boolean flag that defaults to true to return tradable items only.
	 * }
	 */
	public function loadInventory($options) {

		$query = array();

		if ( array_key_exists('language', $options) ) $query['l'] = $options['language'];

		if ( array_key_exists('tradableOnly', $options) ) $query['trading'] = $options['tradableOnly'];

		$contextId = $options['contextId'];

		$uri = 'https://steamcommunity.com/my/inventory/json/'.$options['appId'].'/' .$options['contextId'].'/';

		unset($options);

		$options['headers'] = $this->mobileHeaders;
		$options['cookies'] = $this->jar;
		$options['query'] = $query;

		# dump_r($options);

		try {
			return $this->_loadInventory(array(), $uri, $options, $contextId, null);
		} catch (\Exception $e) {
			throw new CustomException('Inventory loading failed.', $e);
		}
	}

	private function _loadInventory($inventory, $uri, $options, $contextid, $start = null) {

		$options['uri'] = $uri;

		if ($start) $options['query']['start'] = $start;

		$response = $this->client->request('GET', $uri, $options);

		// dump_r( [ $response->getStatusCode(), (string)$response->getBody() ] );

		if($response->getStatusCode() != 200)
			throw new Exception("Error loading inventory. Server response code: ".$response->getStatusCode());

		$response = json_decode($response->getBody());

		if(!$response || !isset($response->rgInventory) || !isset($response->rgDescriptions))
			throw new Exception("Invalid Response");
		
		$inventory = array_merge($inventory,array_merge(
			$this->mergeWithDescriptions($response->rgInventory, $response->rgDescriptions, $contextid),
			$this->mergeWithDescriptions($response->rgCurrency, $response->rgDescriptions, $contextid)
		));
		
		if($response->more) {
			return $this->_loadInventory($inventory, $uri, $options, $contextid, $response->more_start);
		} else {
			return $inventory;
		}
	}

	private function mergeWithDescriptions($items, $descriptions, $contextid) {
		$descriptions = (array) $descriptions;
		$n_items = array();
		foreach ($items as $key => $item) {
			$description = (array) $descriptions[$item->classid.'_'.($item->instanceid ? $item->instanceid : 0)];
			$item = (array) $item;
			foreach ($description as $k => $v) {
				$item[$k] = $description[$k];
			}
			$item['contextid'] = $contextid;
			$n_items[] = $item;
		}
		return $n_items;
	}

}