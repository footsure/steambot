<?php 

namespace App\Steam;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

use Exception;

class SteamTOTPService {

    private static $client = null;
    private static $request = null;

    public static function time($timeOffset) {
        return time() + ($timeOffset || 0);
    }

    public static function getAuthCode($secret, $timeOffset = 0) {
        $secret = self::bufferizeSecret($secret);

        $time = self::time($timeOffset);

        $result = null;
        
        $data = pack('NNC*', 0x0000000, floor($time / 30) & 0xFFFFFFFF);
        
        if (strlen($data) < 8)
            $data = str_pad($data, 8, chr(0), STR_PAD_LEFT);

        $hash = hash_hmac('sha1', $data, $secret);

        $offset = 2 * hexdec(substr($hash, strlen($hash) - 1, 1));
        
        $binary = hexdec(substr($hash, $offset, 8)) & 0x7fffffff;

        $fullcode = intval($binary);
        
        $chars = '23456789BCDFGHJKMNPQRTVWXY';
        $code = '';
        for($i = 0; $i < 5; $i++) {
            $code    .= $chars{ $fullcode % strlen($chars) };
            $fullcode =  floor( $fullcode / strlen($chars) );
        }
        return $code;
    }

    public static function getConfirmationKey($identitySecret, $time, $tag) {
        $identitySecret = self::bufferizeSecret($identitySecret);

        if ($tag && strlen($tag) > 32) $tag = substr($tag,0,32);

        $data = pack('NNC*', 0x0000000, $time & 0xFFFFFFFF);

        if (strlen($data) < 8) 
            $data = str_pad($data, 8, chr(0), STR_PAD_LEFT);

        $tag = self::strToHex($tag);
        $tag = pack('H*', $tag);

        $data .= $tag;
        $hash = hash_hmac('sha1', $data, $identitySecret);
        $confirmationKey = base64_encode( pack('H*', $hash) );
        return $confirmationKey;
    }

    public static function getTimeOffset( $callback, $sendAsync = false ) {
        // if ( !is_callable($callback) ) return;
        $start = time();
        if ( empty(self::$client) ) self::$client = new Client(['base_uri' => 'https://api.steampowered.com']);
        if ( empty(self::$request) ) self::$request = new Request('POST', '/ITwoFactorService/QueryTime/v1/', [
            'headers' => [
                'Content-Length' => 0,
            ]
        ]);
        $rescb = function(ResponseInterface $response) use ($callback, $start) {
            if( $response->getStatusCode() != 200 ) {
                $callback(new Exception("HTTP error " . $response->getStatusCode()));
                return;
            }
            try {
                $response = json_decode($response->getBody())->response;
            } catch( \ErrorException $e ) {
                $callback(new Exception("Malformed response"));
                return;
            }

            if( empty($response) || empty($response->server_time) ) {
                callback(new Exception("Malformed response"));
                return;
            }

            $end = time();
            $offset = $response->server_time - self::time(0);
            $callback(null, $offset, $end - $start);
        };
        if ($sendAsync) {

            $promise = self::$client->sendAsync(self::$request);
            $promise->then(
                $rescb, #function (ResponseInterface $res) {}
                $callback #function (RequestException $e) {}
            );
            $promise->wait();

        } else {
            try {
                $response = self::$client->send(self::$request);
                $rescb($response);
            } catch (RequestException $e) {
                $callback($e);
            }
        }
    }

    public static function getDeviceID($steamID) {
        $pattern = '/^([a-z0-9]{8})([a-z0-9]{4})([a-z0-9]{4})([a-z0-9]{4})([a-z0-9]{12}).*$/';
        $replace = '\\1-\\2-\\3-\\4-\\5';
        return "android:" . preg_replace($pattern, $replace, hash( 'sha1', $steamID ));
    }

    private static function bufferizeSecret($secret) {
        if( is_string($secret) ) {
            // Check if it's hex
            if( preg_match('/[0-9a-f]{40}/i', $secret) ) {
                return $secret;
            } else {
                // Looks like it's base64
                return base64_decode($secret);
            }
        }
        else throw new \InvalidArgumentException('Function argument must be a string');
    }

    private static function strToHex($string) {
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return strToUpper($hex);
    }
}

