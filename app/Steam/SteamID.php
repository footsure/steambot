<?php

namespace App\Steam;

trait SteamID {
	
	// [ 76561198287237376 => 326971648 ]
	public static function toAccountID($id) {
	    if (preg_match('/^STEAM_/', $id)) {
	        $split = explode(':', $id);
	        return $split[2] * 2 + $split[1];
	    } elseif (preg_match('/^765/', $id) && strlen($id) > 15) {
	        return bcsub($id, '76561197960265728');
	    } else {
	        return $id;
	    }
	}
	// [ 326971648 => 76561198287237376 ]
	public static function toSteamID($id) {
	    if (preg_match('/^STEAM_/', $id)) {
	        $parts = explode(':', $id);
	        return bcadd(bcadd(bcmul($parts[2], '2'), '76561197960265728'), $parts[1]);
	    } elseif (is_numeric($id) && strlen($id) < 16) {
	        return bcadd($id, '76561197960265728');
	    } else {
	        return $id;
	    }
	}

    // converts steamid64 to steamid [ 76561198287237376 => STEAM_0:0:163485824 ]
	public static function SteamId64toSteamID($id) {
	    $authserver = bcsub( $id, '76561197960265728' ) & 1;
	    $authid = (bcsub( $id, '76561197960265728' ) - $authserver ) / 2;
	    return "STEAM_0:$authserver:$authid";
	}
}
