<?php

namespace App\Steam;

use Symfony\Component\DomCrawler\Crawler;
use Cache;
use App\Exceptions\Exception;

trait SteamConfirmationAPI {

	private function confirmRequest ($url, $tag, $jsonResp = false, $params = null) {

		$accountId = $this->model->accountId;
		$steamId64 = self::toSteamID($accountId);
		$steamId = self::SteamId64toSteamID($steamId64);

		$confKey = $this -> getConfirmationKey($tag);

		$params['p'] = SteamTOTPService::getDeviceID($steamId);
		$params['a'] = $steamId64;
		$params['k'] = $confKey['key'];
		$params['t'] = $confKey['time'];
		$params['m'] = "android";
		$params['tag'] = $tag;

		$options['query']   = $params;
		$options['headers'] = $this->mobileHeaders;
		$options['cookies'] = $this->jar;

		// dump_r($params);

	  	$response = $this->client->request('GET', "https://steamcommunity.com/mobileconf/$url", $options);

	  	if($response->getStatusCode() != 200)
			throw new Exception("Error requesting confirmation(s). Server response code: ".$response->getStatusCode());

		if ($jsonResp) return json_decode($response->getBody(), true);
		return (string)$response->getBody();
	}

	private function getConfirmationKey ($tag) {

		$key = sprintf("%s:confKeys:%s", $this->key, $tag);

		if ( in_array($tag, $this->reusable) && Cache::has($key) ) {

			$existing = Cache::get($key);

			if ( (time() - $existing['time']) < (60 * 5) ) {
				return $existing;
			}
		}
		$time    = SteamTOTPService::time(0);
		$confKey = SteamTOTPService::getConfirmationKey($this->model->identitySecret, $time, $tag);
		$result  = ['time' => $time, 'key' => $confKey];

		Cache::put($key, $result, 5); return $result;
	}

	/**
	 * Get a list of your account's currently outstanding confirmations.
	 * @param {int} time - The unix timestamp with which the following key was generated
	 * @param {string} key - The confirmation key that was generated using the preceeding time and the tag "conf" (this key can be reused)
	 * @return array $confs
	 */
	public function getConfirmations() {

		$html = $this -> confirmRequest($url = "conf", $tag = "conf");
		$crawler = new Crawler($html);

		$noconfs = $crawler->filter('#mobileconf_empty');
		if ($noconfs->count())
			if (substr_count($noconfs->attr('class'), 'mobileconf_done')) {
				return [];
			}else throw new Exception("Error requesting confirmations list. ".$noconfs->first()->filter('div:nth-of-type(2)')->text());

		$confs = $crawler->filter('#mobileconf_list');
		if ($confs->count() == 0) throw new Exception('Error requesting confirmations list: Malformed response');

		$confs->filter('.mobileconf_list_entry')->each(function (Crawler $node, $i) use (&$results) {

			$conf = $node->extract(['data-confid', 'data-key']);
			list($conf['id'], $conf['key']) = $conf[0]; unset($conf[0]);

			$conf['title']     = trim($node->filter('.mobileconf_list_entry_description>div:nth-of-type(1)')->text());
			$conf['receiving'] = trim($node->filter('.mobileconf_list_entry_description>div:nth-of-type(2)')->text());
			$conf['time']      = trim($node->filter('.mobileconf_list_entry_description>div:nth-of-type(3)')->text());

			# $img = $node->filter('.mobileconf_list_entry_icon img')->first();
			# if ($img->count() == 1) $conf['img'] = $img->attr('src');

			$results[] = $conf;
		});
		// dump_r($results); 
		return $results;
	}

	/**
     * Get the trade offer ID of a confirmation.
     * @param string $confId
     * @return string
     */
    public function getConfirmationTradeOfferId ($confId) {

    	$json = $this -> confirmRequest($url = "details/$confId", $tag = "details", $jsonResp = true);

    	// dump_r($json);

    	if (!empty($json) && isset($json['success']) && $json['success']) {

    		$html = $json['html'];

            if (preg_match('/<div class="tradeoffer" id="tradeofferid_(\d+)" >/', $html, $matches)) {
                return $matches[1];
            }
    	} else return '0';
    }

    public function acceptConfirmation ($conf) {
        return $this->_sendConfirmation($conf, 'allow');
    }
    public function cancelConfirmation ($conf) {
        return $this->_sendConfirmation($conf, 'cancel');
    }

	/**
	 * @param array $conf {
 	 *     Confirmation parameters.
 	 *     @type string $key
 	 *     @type string $id
	 * }
	 * @param string $op 'allow' or 'cancel'
	 * @return bool 
	 */
    private function _sendConfirmation ($conf, $op) {

    	$params['op' ]  = $op;
    	$params['cid']  = $conf['id'];
    	$params['ck' ]  = $conf['key'];

    	$json = $this -> confirmRequest($url = "ajaxop", $tag = $op, $jsonResp = true, $params);

    	return (!empty($json) && isset($json['success']) && $json['success']);
    }
}