<?php 

namespace App\Steam;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Cookie\CookieJar;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

use Cache;
use App\Exceptions\Exception;
use App\Exceptions\CustomException;

class SteamAPIService {

	use SteamInventoryAPI;
	use SteamTradingAPI;
	use SteamConfirmationAPI;
	use SteamID;

	private $options = [];
	private $client;
	private $jar;
	private $sessionId;
	private $captchaGid =  -1;
	  const appId       = 730;
	  const contextId   =   2;
	private $mobileHeaders = [
		"X-Requested-With" => "com.valvesoftware.android.steam.community",
		"User-Agent"       => "Mozilla/5.0 (Linux; U; Android 4.1.1; en-us; Google Nexus 4 - 4.1.1 - API 16 - 768x1280 Build/JRO03S) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
		"Accept"           => "text/javascript, text/html, application/xml, text/xml, */*"
	];

	private $reusable = ['conf', 'details'];
	
	private $key;
	private $apiKey;

	static $exteriors = [
        'factory_new'    => ' (Factory New)',
        'minimal_wear'   => ' (Minimal Wear)',
        'field_tested'   => ' (Field-Tested)',
        'well_worn'      => ' (Well-Worn)',
        'battle_scarred' => ' (Battle-Scarred)'
    ];

    static $offerStates = [
        'k_ETradeOfferStateInvalid'                  => 'invalid', //Invalid
        'k_ETradeOfferStateActive'                   => 'sent', //This trade offer has been sent, neither party has acted on it yet.
        'k_ETradeOfferStateAccepted'                 => 'accepted', //The trade offer was accepted by the recipient and items were exchanged.
        'k_ETradeOfferStateCountered'                => 'countered', //The recipient made a counter offer
        'k_ETradeOfferStateExpired'                  => 'expired', //The trade offer was not accepted before the expiration date
        'k_ETradeOfferStateCanceled'                 => 'canceled', //The sender cancelled the offer
        'k_ETradeOfferStateDeclined'                 => 'declined', //The recipient declined the offer
        'k_ETradeOfferStateInvalidItems'             => 'invalid', //Some of the items in the offer are no longer available (indicated by the missing flag in the output)
        'k_ETradeOfferStateCreatedNeedsConfirmation' => 'created', //The offer hasn't been sent yet and is awaiting email/mobile confirmation. The offer is only visible to the sender.
        'k_ETradeOfferStateCanceledBySecondFactor'   => 'canceled', //Either party canceled the offer via email/mobile. The offer is visible to both parties, even if the sender canceled it before it was sent.
        'k_ETradeOfferStateInEscrow'                 => 'hold' //The trade has been placed on hold. The items involved in the trade have all been removed from both parties' inventories and will be automatically delivered in the future.
    ];

	const USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36";

	public function __construct(\App\Bot $model) {

		$this->model = $model;

		$this->client = new Client();

		$this->key = "steambot#{$model->id}";

		$key = "steambot#{$model->id}:cookieJar";

		if (!Cache::has($key)) {

			$this->jar = CookieJar::fromArray(['Steam_Language'=>'english', 'timezoneOffset'=>'0,0'], 'steamcommunity.com');
			foreach ($this->jar->getIterator() as $cookie) { $cookie->setSecure(true); $cookie->setDiscard(false); }

			$details = [
				'accountName'   => $this->model->accountName, // 'betdrop1'
				'password'      => $this->model->password, // 'pkR4s2GYEcWZCp'
				'twoFactorCode' => SteamTOTPService::getAuthCode($this->model->sharedKey) // '+7x47ly0Ktkuxmaai7rq6+aPAMs='
			];

			try {
				$this->login($details);
			} catch (\Exception $e) {
				throw new CustomException('Login failed.', 100, $e);
			}
			$this->sessionId = bin2hex(random_bytes(12));
			$this->jar->setCookie(new SetCookie(['Name' => 'sessionid', 'Value' => $this->sessionId, 'Domain' => 'steamcommunity.com', 'Secure' => true ]));
			Cache::put($key, $this->jar, 30);
			Cache::put("{$this->key}:sessionId", $this->sessionId, 30);
		   
		} else {
			$this->sessionId = Cache::get("{$this->key}:sessionId");
			$this->jar       = Cache::get($key);
		}
		// dump_r($this->jar->toArray());
	}

	public function login($details) {

		// throw new Exception("Test exception");

		if(isset($details['steamguard'])) {
			$parts = split('||', $details['steamguard']);
			$this->jar->setCookie(new SetCookie(['Name' => "steamMachineAuth{$parts[0]}", 'Value' => urlencode($parts[1]), 'Domain' => 'steamcommunity.com']));
		}
		$referer = ["Referer" => "https://steamcommunity.com/mobilelogin?oauth_client_id=DE45CD61&oauth_scope=read_profile%20write_profile%20read_client%20write_client"];
		$options['headers'] = $this->mobileHeaders + $referer;

		$this->jar->setCookie(new SetCookie(['Name' => 'mobileClientVersion' , 'Value' => '0 (2.1.3)' , 'Domain' => 'steamcommunity.com', 'Secure' => true ]));
		$this->jar->setCookie(new SetCookie(['Name' => 'mobileClient'        , 'Value' => 'android'   , 'Domain' => 'steamcommunity.com', 'Secure' => true ]));
		
		$options['cookies'] = $this->jar;

		$options['form_params'] = ['username' => $details['accountName'] ];

		try {
			$response = $this->client->request('POST', 'https://steamcommunity.com/login/getrsakey/', $options);
			// dump_r( [ $response->getStatusCode(), (string)$response->getBody() ] );
			$body = json_decode($response->getBody(), true);
		} catch (RequestException $e) {
			$this->deleteMobileCookies(); throw $e;
		}

		if(!$body['publickey_mod'] || !$body['publickey_exp']) {
			$this->deleteMobileCookies();
			throw new Exception("Invalid RSA key received");
		}

		$RSA = new \phpseclib\Crypt\RSA();
		$N = new \phpseclib\Math\BigInteger($body['publickey_mod'], 16);
		$E = new \phpseclib\Math\BigInteger($body['publickey_exp'], 16);
		$KEY = $RSA->_convertPublicKey($N, $E);
		# dump_r($KEY); 
		$RSA->loadKey($KEY);
		$RSA->setPublicKey();
		$RSA->setEncryptionMode(\phpseclib\Crypt\RSA::ENCRYPTION_PKCS1);
		$ciphertext = $RSA->encrypt($details['password']);
		# dump_r($ciphertext);
		$ciphertext = bin2hex($ciphertext);
		# dump_r($ciphertext);

		$options['form_params'] = [
			'captcha_text'      => isset($details['captcha']) ? $details['captcha'] :  '',
			'captchagid'        => $this->captchaGid,
			'emailauth'         => isset($details['authCode']) ? $details['authCode'] : '',
			'emailsteamid'      => '',
			'password'          => base64_encode( pack('H*', $ciphertext) ),
			'remember_login'    => 'true',
			'rsatimestamp'      => $body['timestamp'],
			'twofactorcode'     => isset($details['twoFactorCode']) ? $details['twoFactorCode'] : '',
			'username'          => $details['accountName'],
			'oauth_client_id'   => 'DE45CD61',
			'oauth_scope'       => 'read_profile write_profile read_client write_client',
			'loginfriendlyname' => '#login_emailauth_friendlyname_mobile',
			'donotcache'        => time()*1000
		];

		// dump_r($options['form_params']); 

		try {
			$response = $this->client->request('POST', 'https://steamcommunity.com/login/dologin/', $options);
			// dump_r( [ $response->getStatusCode(), (string)$response->getBody() ] );
			$body = json_decode($response->getBody(), true);
		} catch (RequestException $e) {
			$this->deleteMobileCookies(); throw $e;
		}
		if ( !$body['success'] ) throw new Exception($body['message']);
	}

	private function deleteMobileCookies() {
		$this->jar->clear('steamcommunity.com', '/', 'mobileClientVersion');
		$this->jar->clear('steamcommunity.com', '/', 'mobileClient');
	}
}