<?php

namespace App\Log;

use Monolog\Formatter\LineFormatter as MonologLineFormatter;

class LineFormatter extends MonologLineFormatter
{
    public function format(array $record)
    {   
        $output = parent::format($record);

        return $output;
    }
}
