<?php

namespace App\Jobs;

use App\Offer;
use Cache;

class CreateTradeOffer extends BaseSteamAPIJob {

    protected $jobinfo = '> Creating TradeOffer';
    protected $messsage;

    public function __construct(Offer $offer, $role, $messsage = '') {

        parent::__construct($offer, $role);
        $this->messsage = $messsage;
    }

    public function main() {

        $offer      = $this -> offer;
        $user       = $this -> getUser();
        $recipient  = $this -> getRecipient();
        $steam      = $user -> getSteamAPIService();

        if ($this->role == 'mediator') {

            $key = $this->rkey.'>new_market_instance_id';
            $offer->item_market_instance_id = Cache::remember($key, 30, function() use ($steam, $offer) { 

                $items = $steam -> loadInventory(['appId' => $steam::appId, 'contextId' => $steam::contextId]);

                foreach ($items as $item)
                    if ($item['market_hash_name'] == $offer->item_market_hash_name)
                        return $item['id'];
            });
        }


        $itemsFromMe = [
            [
                "appid"        =>   $steam::appId,
                "contextid"    =>   $steam::contextId,
                "amount"       =>   1,
                "assetid"      =>   $offer->item_market_instance_id
            ]
        ];

        $itemsFromThem = [];

        $options = [
            'partnerAccountId' =>   $recipient->accountId,
            'accessToken'      =>   $recipient->token,
            'itemsFromMe'      =>   $itemsFromMe,
            'itemsFromThem'    =>   $itemsFromThem,
        ];

        if (!empty($this->messsage)) $options['message'] = $this->messsage;

        $tradeOffer            =    $steam -> makeOffer($options);

        $offer->offerId        =    $tradeOffer->tradeofferid;
        $offer->stateCode      =    'k_ETradeOfferStateCreatedNeedsConfirmation';
        $offer->status         =    $steam::$offerStates[$offer->stateCode];

        $user->unsetSteamAPIService(); // or $this->getUser()->unsetSteamAPIService();

        $job = (new AcceptConfirmation($offer, $user->type))->onQueue('tradeoffers')->delay(10);

        dispatch($job);

        return true;
    }
}
