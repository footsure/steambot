<?php

namespace App\Jobs;

use App\Exceptions\Handler;
use App\Bot;
use Cache;
use Log;

class CheckSentTradeOffers extends Job {

    protected $timeout = 5*60;
    protected $rkey    = null;
    protected $userId  = null;
    protected $user    = null;
    protected $report  = null;

    public function __construct(Bot $user) {

        $user->unsetSteamAPIService();
        $this->userId = $user->id;
        $this->rkey   = 'users:'.$user->id.':user';
        Cache::put($this->rkey, $user, 60);
    }

    public function handle() {

        try {

            $this->user = Cache::remember($this->rkey, 60, function() { 
                return Bot::find($this->userId);
            });

            $user = $this->user;

            $steam = $user -> getSteamAPIService();

            $offers = $user -> offers() -> whereIn('status', ['sent', 'hold']) -> get();

            if ($offers->count() == 0) {
                $this->delete(); return;
            }

            $rkey = 'users:'.$user->id.':sent_trade_offers';
            $minutes = (int) $this->timeout/60;
            $tradeOffers = Cache::remember($rkey, $minutes, function() use ($steam) { 
                return $steam -> getSentOfferHistory();
            });

            if (empty($tradeOffers)) { 
                $this -> release($this->timeout); 
                return; 
            }

            foreach ($offers as $offer) {

                if (array_key_exists($offer->offerId, $tradeOffers)) {

                    $oid                     =  $offer->offerId;
                    $tradeOffer              =  $tradeOffers[$oid];

                    if ($offer->state      !==  $tradeOffer['state']) {

                        $this->report[]      =  sprintf('#%s : %s > %s', $offer->offerId, $offer->status, $tradeOffer['status']);

                        $offer->status       =  $tradeOffer['status'];
                        $offer->stateCode    =  $tradeOffer['state'];

                        $linkedOffers        =  $offer->linkedOffers;
                        $linkedOffers[$oid]  =  $tradeOffer;
                        $offer->linkedOffers =  $linkedOffers;

                        $offer->save();

                        if ( in_array($offer->status, ['accepted']) && $user->type == 'mediator' ) {

                            if (count($offer->linkedOffers) > 1) {

                                $linkedOffers = $offer->linkedOffers;

                                $tradeOffer = reset($linkedOffers);

                                if (isset($tradeOffer['item_market_instance_id'])) {

                                    $item_market_instance_id = $tradeOffer['item_market_instance_id'];

                                    $pivot_table = $user->items()->getTable();
                                    $row = \DB::table($pivot_table) -> where( 'item_instance_id', $item_market_instance_id ) -> delete();
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($this->report)) {

                $this->report = implode(' | ', $this->report);
                $this->log($this->report);
            }

            $this -> release($this->timeout);
            
        } catch (\Exception $e) {

            $this -> release($this->timeout);

            $handler = new Handler;
            $e = new JobException($this->info(), 101, $e);
            $handler -> report($e);
            
        }
        unset($this->user);
    }

    public function failed() {

    }

    public function log($message = '') {

        Log::info($this->info().$message);
    }

    private function info() {

        return sprintf('[ bot#%s > checking sent tradeoffers state ] ', $this->user->accountId); 
    }
}
