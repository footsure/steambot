<?php

namespace App\Jobs;

class AcceptConfirmation extends BaseSteamAPIJob {

    protected $jobinfo = '> TradeOffer Confirmation';

    public function main() {

        $offer = $this -> offer;
        $user  = $this -> getUser();
        $steam = $user -> getSteamAPIService();

        $confirnations = $steam -> getConfirmations();

        if (empty($confirnations)) { $this -> release($this->timeout()); return; }

        if ($steam -> acceptConfirmation($confirnations[0])) {

            $offer->stateCode = 'k_ETradeOfferStateActive';

            $offer->status = $steam::$offerStates[$offer->stateCode];

            // $offer->save();

            // echo 'offer #'.$offer->offerId.' was confirmed';

            if ($this->role == 'proprietor') {

                $user->unsetSteamAPIService();

                $job = (new AcceptTradeOffer($offer, $this->getRecipient()->type))->onQueue('tradeoffers')->delay(10);

                dispatch($job);
            }

            else { // testing incoming offer 

                $user->unsetSteamAPIService();

                $incoming = new \App\Offer([
                    'status'                  => $offer->status,
                    'stateCode'               => $offer->stateCode,
                    'type'                    => 'incoming',
                    'partnerAccountId'        => $user->accountId,
                    // 'accessToken'             => $user->token,
                    'item_market_hash_name'   => $offer->item_market_hash_name,
                    'item_market_instance_id' => $offer->item_market_instance_id,
                ]);

                $incoming -> owner() -> associate($offer->owner);
                $incoming -> item()  -> associate($offer->item);

                $incoming -> save();
                $incoming =  $incoming -> fresh(['owner', 'mediator', 'item']);

                $job = (new AcceptTradeOffer($incoming, 'proprietor'))->onQueue('tradeoffers')->delay(10);

                dispatch($job);

                $job = (new CheckSentTradeOffers($user))->onQueue('tradeoffers')->delay(2*60);

                dispatch($job);
            }

            return true;

        } else { $this -> release($this->timeout()); return false; }
    }

}
