<?php

namespace App\Jobs;

use App\Offer;
use Cache;
use Log;

use App\Exceptions\Handler;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class BaseSteamAPIJob implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $oid;
    protected $offer;
    protected $role;
    protected $rkey;
    protected $timeout = 10;
    protected $jobinfo = '';
    protected $attempts = 10;
    protected $multiplier = 30; // delay = timeout + attempts x multiplier

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Offer $offer, $role)
    {
        $this->role = $role;
        $this->oid  = $offer->id;
        $this->rkey = 'offers:'.$this->oid.':tradeoffer';
        Cache::put($this->rkey, $offer, 60);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->offer = Cache::remember($this->rkey, 60, function() { 
            return Offer::with('owner', 'mediator', 'item')
                        ->find($this->oid); 
        });

        if ($this->attempts() > $this->attempts) $this->fail();

        $offer = $this->offer;

        try {

            if ( $this->main() ) {

                $this->log($this->jobinfo.' [successful]');
                $offer->save();
            }
            else $this->log($this->jobinfo.' [failed|retry]');

        } catch (\Exception $e) {

            $handler = new Handler;
            if ($this->attempts() > $this->attempts) $this->fail();
            else $this->release($this->timeout());
            $message = sprintf('%s%s (%s)', $this->info(), $this->jobinfo, $this->attempts());
            $e = new JobException($message, 101, $e);
            $handler -> report($e);
        }
        unset($this->offer);
    }

    protected function main() {}

    protected function timeout() { 

        return $this->timeout + rand(1, 60) + $this->multiplier * $this->attempts(); 
    }

    public function getUser() {

        switch ($this->role) {
            case 'proprietor': return $this->offer->owner;
            case 'mediator'  : return $this->offer->mediator;
            default          : throw  new \Exception("Unknown user role: {$this->role}");
        }
    }

    public function getRecipient() {

        switch ($this->role) {
            case 'proprietor': return $this->offer->mediator;
            case 'mediator'  : return (object)['accountId' => $this->offer->partnerAccountId, 'token' => $this->offer->accessToken];
            default          : throw  new \Exception("Unknown user role: {$this->role}");
        }
    }

    public function log($message = '') {

        Log::info($this->info().$message);
    }

    private function info() {

        $user  = $this->getUser();
        $offer = $this->offer;
        $role  = $this->role;

        if ($offer->type == 'incoming') 
            $relationship = sprintf('from %s to bot#%s', $offer->partnerAccountId, /*$user->accountName,*/ $user->accountId);
        elseif ($role == 'proprietor') 
            $relationship = sprintf('from bot#%s to bot#%s', /*$user->accountName,*/ $user->accountId, /*$offer->mediator->accountName,*/ $offer->mediator->accountId);
        elseif ($role == 'mediator')  
            $relationship = sprintf('from bot#%s to %s', /*$user->accountName,*/ $user->accountId, $offer->partnerAccountId);

        $offerinfo = !empty($offer->offerId) ? sprintf('tradeoffer %s ', $offer->offerId) : '';

        return sprintf('[%s%s] ', $offerinfo, $relationship); 
    }

    protected function fail() {

        $this->delete();

        #$class = get_class($this);

        #$classes = ['CreateTradeOffer', 'AcceptConfirmation', 'AcceptTradeOffer'];

        #if ($this->role == 'proprietor')
            #$this->offer->owner->items()->updateExistingPivot($this->offer->item->id, ['available' => true]);

        $this->offer->status = 'failed';
        $this->offer->save();
        return;

    }
}
