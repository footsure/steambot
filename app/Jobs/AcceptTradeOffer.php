<?php

namespace App\Jobs;

use Cache;

class AcceptTradeOffer extends BaseSteamAPIJob {

    protected $jobinfo = '> Acceptance TradeOffer';

    public function main() {

        $offer = $this -> offer;
        $user  = $this -> getUser();
        $steam = $user -> getSteamAPIService();

        $tradeOffers = $steam->getIncomingOffers();
        $oid         = $offer->offerId;

        if (empty($tradeOffers)) { 

            $this -> release($this->timeout()); 
            return; 
        }

        if ($this->role == 'proprietor') {

            $tradeOffer     = reset($tradeOffers);
            $oid            = $tradeOffer['id'];
            $offer->offerId = $oid;

        } else {

            if (!isset($tradeOffers[$oid])) { 
                $this -> release($this->timeout()); 
                return; 
            }
            $tradeOffer = $tradeOffers[$oid];
        }

        $offer->stateCode    = $tradeOffer['state'];
        $offer->status       = $tradeOffer['status'];

        if (empty($offer->linkedOffers)) 
              $linkedOffers  = [];
        else  $linkedOffers  = $offer->linkedOffers;

        $lhash               = 'item_market_instance_id';
        $tradeOffer[$lhash]  = $offer->item_market_instance_id;
        $linkedOffers[$oid]  = $tradeOffer;
        $offer->linkedOffers = $linkedOffers;

        if ($offer->status == 'sent') {

            $body = $steam -> acceptOffer($oid, $tradeOffer['otherAccountId']);

            $offer->stateCode = 'k_ETradeOfferStateAccepted';
            $offer->status    = $steam::$offerStates[$offer->stateCode];

            if ($this->role == 'mediator') {

                $user->unsetSteamAPIService();

                $job = (new \App\Jobs\CreateTradeOffer($offer, $user->type))->onQueue('tradeoffers')->delay(10);

                dispatch($job);

            } else $user -> updateInventory(); 

            return true;
        } 
        else { $this -> release($this->timeout()); return false; }
    }
}
