<?php

namespace App\Exceptions;

use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Helper\TableSeparator;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    protected $exclude_from_tracestack = 
    [
        'namespaces' => ['\Middleware\\'],
        'filepaths'  => ['\Middleware\\']
    ];

    protected $include_to_tracestack = 
    [
        'namespaces'  => ['App\\'],
        'filepaths'   => ['\steambot\app']
    ];

    const EOL   = "\n";
    const ZEROL =   "";

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(\Exception $e)
    {
        if ($this->shouldntReport($e)) {
            return;
        }

        try {
            $logger = app('Psr\Log\LoggerInterface');
        } catch (Exception $ex) {
            throw $e; // throw the original exception
        }

        if ($e instanceof \App\Jobs\JobException) {
            $err = $e->getMessage().self::EOL;
            $e = $e->getPrevious();
        } else $err = self::ZEROL;

        $stacktrace[] = $e->getTrace(); 
            $errors[] = $e; 

        if ($previous = $e->getPrevious())
            do {
                 $stacktrace[] = $previous->getTrace();
                     $errors[] = $previous;
            } while ($previous = $previous->getPrevious());

              $err .= (string) $e;
        $stacktrace = array_reverse($stacktrace);
            $errors = array_reverse($errors);

        $output = new BufferedOutput;
        $table  = new Table($output);

        $lines  = explode("\n", $err);

        while (true) {

            $a = $b = 0;

            foreach ($lines as $n => $line) {

                if ( starts_with ( $line, 'Stack'  )) $a = $n;
                if (   ends_with ( $line, '{main}' )) $b = $n;

                if ($b > $a && $a > 0) { 

                    array_splice($lines, $a, $b - $a + 1, '#debuginfo#'); 

                    continue 2; 
                }
            }
            break 1;
        }

        $rootpath = preg_quote(app()->basePath()); 
        $regex    = "/(in $rootpath\D*:\d+)/"; 

        foreach ($lines as &$line) {

            preg_match($regex, $line, $matches);

            if (!empty($matches)) {

                $line   = rtrim ( preg_replace( $regex, "", $line ), ':' );
                $locs[] = preg_replace( "/($rootpath)/", "", $matches[0] );
            }
        }

        $lines = collect($lines)->filter(function ($line) use (&$delete) {

            if ($line == '#debuginfo#') $delete = false;
            if ($delete) return false;
            if (ends_with( $line, 'response:')) $delete = true;
            return true;

        })->all();

        foreach ($stacktrace as $i => $trace) {

            if (is_array($trace)) $trace = collect($trace);

            if ($trace instanceof \Illuminate\Support\Collection) {

                $keymap   = ['class' => 'namespaces', 'file' => 'filepaths'];
                $headers  = ['#', 'Class', 'Function', 'File'];
                $rootpath = app()->basePath();

                $trace = $trace->filter( function ($item) use ($keymap) {
                    
                    foreach ( $keymap as $key => $type ) if ( array_key_exists($key, $item) ) {

                        foreach ($this->exclude_from_tracestack[$type] as $pattern)
                            if (str_contains($item[$key], $pattern))
                                return false;

                            if (empty($this->include_to_tracestack[$type])) 
                                return true;

                        foreach ($this->include_to_tracestack[$type] as $pattern)
                            if (str_contains($item[$key], $pattern))
                                return true;
                    }

                })->map( function($item, $key) use ($headers, $rootpath) {

                    $new[] = $key;
                    if (isset($item['file'])) $item['file'] = substr($item['file'], strlen($rootpath));
                    for ($i=1; $i < count($headers); $i++) { 
                        $key = strtolower($headers[$i]);
                        if (isset($item[$key])) $new[] = $item[$key];
                        else $new[] = '';
                    } return $new;

                })->all();

                if (!empty($trace)) $table->setHeaders($headers)->setRows($trace)->setStyle('default')->render();
            } 

            $e = $errors[$i];

            if (method_exists($e, 'getRequest')) {

                $req = $e->getRequest();

                if (method_exists($req, 'getUri'))
                    $uri = (string) $req->getUri();

                if (method_exists($req, 'getMethod')){
                    $method = $req->getMethod();
                    if ($method == 'POST' && method_exists($req, 'getBody'))
                        $post_form_params = urldecode($req->getBody());
                }
                if (isset($uri) && isset($method)) {

                    $rows = [['URI:', sprintf('%s %s', $method, $uri)]];

                    if (isset($post_form_params))
                        $rows[] = ['DATA:', $post_form_params];

                    $rows[] = ['   ','   '];

                    $table->setHeaders([])->setRows($rows)->setStyle('compact')->render();
                }
            }

            $index = array_search('#debuginfo#', $lines);

            $lines [ $index ] = $output->fetch() . $locs[$i];

            // r($lines [ $index ]);

        }

        $err = implode("\n", $lines).PHP_EOL;

        // r($lines); 

        $logger->error($err);
        // parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Exception $e)
    {
        if ($e instanceof CustomException)
            return response()
                    ->json($e->toArray())
                    ->setStatusCode(500);

        $response = parent::render($request, $e);
        $response->exception = null;
        return $response;
    }
}
