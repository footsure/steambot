<?php

namespace App\Exceptions;

use Illuminate\Http\Exception\HttpResponseException as BaseHttpResponseException;

class HttpResponseException extends BaseHttpResponseException {
    
    public function __construct($message = '', \Exception $e, $json = true) {

    	if (!empty($message)) $message = "$message ";

    	if (env('APP_DEBUG', false) || !$json)
	        $this->response = response(['success' => false, 'message' => $message.$e->getMessage()], 500) -> header('Content-Type', 'text/html') -> withException($e);
        else 
	        $this->response = response(['success' => false, 'message' => $message.$e->getMessage()], 500) -> withException($e);
    }

}