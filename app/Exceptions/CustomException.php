<?php 

namespace App\Exceptions;

 class CustomException extends Exception {

	public function toArray () {

		return ['success' => false, 'message' => $this->getMessage()];
	}
}