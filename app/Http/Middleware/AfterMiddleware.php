<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\Handler;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // if ($response->isServerError()) ob_end_clean();
        /*else */ob_end_flush();

        if (isset($response->exception)) {

            $handler = new Handler;
            $handler -> report($response->exception);
            
        	#app('log')->error($response->exception);
        } 

        return $response;
    }
}