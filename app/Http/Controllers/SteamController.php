<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exceptions\CustomException;
use App\Exceptions\HttpResponseException;
use App\Steam\SteamAPIService;

class SteamController extends Controller
{
    /**
     *
     * @param  Request  $request
     * @return Response
     */
    public function dispatchOffersTask(Request $request) {

        if ($request->has('action')) {

            $action = $request->input('action');

            if ($action == 'create') try {

                extract( $this->getParams( ['itemId', 'partnerAccountId', 'accessToken'] ) );

                $item = \App\Item::find($itemId);

                if (is_null($item)) throw new CustomException("Wrong ID. No items with this id in items list");

                $bot = $item ->bots() ->where('type', 'proprietor') ->wherePivot('available', true) ->distinct() ->groupBy('id') ->orderBy('updated_at', 'asc') ->first();

                if (!$bot) throw new CustomException("No bots having this item");

                // $bot->items()->updateExistingPivot($itemId, ['available' => false]);

                $bot->pivot->available = 0;

                $bot->items()->newPivotStatement()->where('item_instance_id', $bot->pivot->item_instance_id)->update(['available' => false]);

                // dump_r($bot->toJson()); return;

                $recipient = \App\Bot::where('type', 'mediator') ->orderBy('updated_at', 'asc') -> first();

                // dump_r($recipient->toJson());

                // $accountId = SteamAPIService::toAccountID($bot->accountId);

                // $steamId64 = SteamAPIService::toSteamID($bot->accountId);
                
                // $steamId   = SteamAPIService::SteamId64toSteamID($steamId64);

                // dump_r([$accountId, $steamId64, $steamId]);

                // r([$action, $itemId, $partnerAccountId, $accessToken, $item]);

                $offer = new \App\Offer([
                    'status'                  => 'queued',
                    'partnerAccountId'        => $partnerAccountId,
                    'accessToken'             => $accessToken,
                    'item_market_hash_name'   => $item->hash_name . SteamAPIService::$exteriors[$item->exterior],
                    'item_market_instance_id' => $bot->pivot->item_instance_id,
                ]);

                $offer -> owner()    -> associate($bot);
                $offer -> mediator() -> associate($recipient);
                $offer -> item()     -> associate($item);

                $offer -> save();
                $offer =  $offer -> fresh(['owner', 'mediator', 'item']);

                $job = (new \App\Jobs\CreateTradeOffer($offer, $bot->type))->onQueue('tradeoffers')->delay(20);

                $this->dispatch($job); 

                return response(["success" => true, "offer" => $offer->id]);

            } catch (\Exception $e) {
                if ($bot) $bot->items()->newPivotStatement()->where('item_instance_id', $bot->pivot->item_instance_id)->update(['available' => true]);
                throw new HttpResponseException("Error creating offer.", $e);
            }
        }
    }

    /**
     *
     * @param  Request  $request
     * @return Response
     */
    public function dispatchBotsTask(Request $request) {

        if ($request->has('action')) try {

            $action = $request->input('action');

            if ($action == 'update') {

                $accountName = $request->input('accountName');

                $updated = [];

                $updateInventory = function($bot) use (&$updated) { $updated[$bot->accountName] = $bot->updateInventory(); };

                if (empty($accountName)) {
                    $bots = \App\Bot::whereNull('updated_at')->get()->each($updateInventory);
                } else {
                    $bots = \App\Bot::where('accountName', $accountName)->get()->each($updateInventory);
                    if ($bots->count() == 0) throw new CustomException("Bot not found");
                } 
                return response([ 'success' => true, 'updated' => $updated ]);
            }
        } catch (\Exception $e) {
            throw new HttpResponseException("Error updating offer(s).", $e);
        }
    }

    /**
     *
     * @param  array  $keys
     * @return array
     */
    public function getParams($keys) {

        $params = app('request')->intersect($keys); // return array_filter($request->only($keys)); // return only elements with not empty values

        // if ( count($params) == 3 ) list($itemId, $partnerSteamId, $accessToken) = array_values($params); // the number of list() arguments must be equal to or less than the number of array elements

        if ( count($params) == count($keys) ) return $params;
        else {
            $missing = implode(', ', array_values(array_diff($keys, array_keys($params))));
            throw new CustomException("Not enough parameters for this action. Missing query string parameters: $missing");
        }
    }
}
