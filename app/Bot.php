<?php

namespace App;

use App\Steam\SteamAPIService;
use Illuminate\Database\Eloquent\Builder;
use App\Scopes\CustomGlobalScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bot extends Model {

    protected $table   = 'bots';
    protected $steam   =  null ;

    use SoftDeletes;

    /*The attributes that should be mutated to dates.*/
    protected $dates = ['deleted_at'];

    // protected $hidden = ['pivot'];

    public function items() {
        return $this->belongsToMany('App\Item', 'bots_items', 'bots_id', 'items_id')
                    ->withPivot(['available', 'item_instance_id'])
                    ->withTimestamps();
    }   

    public function getSteamAPIService() {
        if (!isset($this->steam))
            $this->steam = new SteamAPIService($this);
        return $this->steam;
    }

    public function unsetSteamAPIService() {
        $this->steam = null;
    }

    public function offers() {
        if ($this->type == 'proprietor')
        return $this->hasMany('App\Offer', 'own_id');
        if ($this->type == 'mediator')
        return $this->hasMany('App\Offer', 'mediator_id');
    }

    public function updateInventory() {

        $steam = $this -> getSteamAPIService();

        $inventory = $steam -> loadInventory(['appId' => SteamAPIService::appId, 'contextId' => SteamAPIService::contextId]);

        // dump_r($inventory);

        $related = $this -> items() -> wherePivot('available', true) -> get(['id', 'hash_name', 'exterior', 'item_instance_id']);
        
        $related = $related -> keyBy('item_instance_id');

        // dump_r($related);

        $stocked = collect( array_column($inventory, 'market_hash_name', 'id') ); // market_hash_name : {name} ({exterior})
       
        // dump_r($stocked);

        $deleted = $related   -> diffKeys($stocked);
        $added   = $stocked   -> diffKeys($related);

        // dump_r($added);
        // dump_r($deleted);

        $keys = collect(['id', 'market_hash_name', 'name']);

        foreach ($inventory as $item) 

            if ($added->has($item['id']))

                $items[] = $keys -> map(

                    function($key) use ($item) { return $item[$key]; }
                
                ) -> all();

        if (isset($items)) {

            $should_be_added = \App\Item::getMatchedItemsIds($items);

            // dump_r($should_be_added);

            foreach ($should_be_added as $level => $ids) {

                $this -> items() -> attach($ids);

                foreach ($ids as $itemId => $attributes) { // logging results

                   $marketId = $attributes['item_instance_id'];

                   $results['added'][] = [ 'localId'  => $itemId,
                                           'marketId' => $marketId,
                                           'hashName' => $added[$marketId] ];
                }
            }
        } 
        else $results['added'] = 0;

        if ($deleted->count()) {

            $pivot_table = $this->items()->getTable();

            $should_be_deleted = $deleted->keys()->all();

            $rows = \DB::table($pivot_table) -> whereIn( 'item_instance_id', $should_be_deleted );

            foreach ($deleted as $marketId => $item) { // logging results

                $hashName = $item->hash_name . SteamAPIService::$exteriors[ $item->exterior ];

                $results['deleted'][] = [ 'localId'  => $item->id,
                                          'marketId' => $marketId,
                                          'hashName' => $hashName, ];
            }
            
        } 
        else $results['deleted'] = 0;

        return $results;
    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // static::addGlobalScope(new CustomGlobalScope);
    }

}
