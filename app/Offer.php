<?php

namespace App;

class Offer extends Model {

    protected $table = 'offers';

    protected $casts = [
        'linkedOffers' => 'array'
    ];

    public function owner() {
        return $this->belongsTo('App\Bot', 'own_id');
    }

    public function mediator() {
        return $this->belongsTo('App\Bot');
    }

    public function item() {
        return $this->belongsTo('App\Item');
    }

}
