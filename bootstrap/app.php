<?php

//added this so that when you do localdev on a windows box, it doesn't crash when looking for memcached.
if (!class_exists('Memcached')) {
    include ("memcached.php");
}

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
   App\Http\Middleware\BeforeMiddleware::class,	
   // App\Http\Middleware\ExampleMiddleware::class
   App\Http\Middleware\AfterMiddleware::class
]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);

$app->configure('filesystems');

// $app->register(Illuminate\Filesystem\FilesystemServiceProvider::class);

if (env('APP_DEBUG')) {
	// $app->register(Barryvdh\Debugbar\LumenServiceProvider::class);
	// class_alias('Barryvdh\Debugbar\Facade', 'Debugbar');
	// $app->configure('debugbar');
}

if (!env('APP_DEBUG', false)) {
	if (!function_exists('dump_r')) {
		function dump_r($raw, $ret = false, $html = true, $depth = 1e3, $expand = 1e3) {}
	}
	if (!function_exists('r')) {
		function r() {}
	}
} elseif (class_exists('ref')) {

	ref::config('validHtml', true);
	// ref::config('showIteratorContents', true);
	// ref::config('expLvl', 3); // default 1
	ref::config('maxDepth', 8); // default 6
	ref::config('showPrivateMembers', true);
}

$app->configureMonologUsing(function($monolog) {

	// the default date format is "Y-m-d H:i:s"
	// the default output format is "[%datetime%] %channel%.%level_name%: %message% %context% %extra%\n"
	$output = "%datetime% > %level_name% > %message% %context% %extra%\n";
	$formatter = new \Monolog\Formatter\LineFormatter($output, "H:i:s", true, true);

    // $stream = new \Monolog\Handler\StreamHandler(storage_path('logs/lumen.log'), \Monolog\Logger::DEBUG);
    $stream = new \Monolog\Handler\RotatingFileHandler(storage_path('logs/lumen.log'), 0, \Monolog\Logger::DEBUG);
	$stream->setFormatter($formatter);
    return $monolog->pushHandler($stream);
});

date_default_timezone_set(env('TIMEZONE', "Europe/Kiev"));


/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
    require __DIR__.'/../app/Http/routes.php';
});

return $app;
